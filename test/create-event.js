'use strict';

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
chai.use(require('chai-as-promised'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const uuid = require('uuid/v4');
const createEventFactory = require('../src/create-event');
const participantRole = require('../src/include/participant-role');

describe('Creating an Event', function() {
    let dsEmulator,
        createEvent;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        const dsInstance = new Datastore();
        createEvent = createEventFactory(dsInstance);
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should fail if name is not provided', function() {
        return expect(
            createEvent(uuid())
        ).to.be.rejectedWith(TypeError, 'Event name must be provided.');
    });

    it('should fail if time is not provided as ISO-8601 string', function() {
        return expect(
            createEvent(uuid(), 'Test event', {
                time: 'Next Tuesday',
            })
        ).to.be.rejectedWith(TypeError, 'Event time must be provided as a ISO-8601 string.');
    });

    it('should fail if locationId is not provided as UUID', function() {
        return expect(
            createEvent(uuid(), 'Test event', {
                locationId: 123,
            })
        ).to.be.rejectedWith(TypeError, 'EventLocation ID must be provided as an UUID.');
    });

    it('should return a Event with a key property', async function() {
        const event = await createEvent(uuid(), 'Test event');

        expect(event).to.have.property('key');
    });

    it('should return a Event with a data property', async function() {
        const event = await createEvent(uuid(), 'Test event');

        expect(event).to.have.property('data');
    });

    describe('Event key (GCP Datastore)', function() {
        it('should be of the Event kind', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });
            const { kind } = event.key;

            expect(kind).to.equal('Event');
        });

        it('should only contain kind and a UUID', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            // eslint-disable-next-line no-magic-numbers
            expect(event.key.path.length).to.equal(2);
        });

        it('should contain a valid random v4 UUID', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });
            const [ kind, eventId ] = event.key.path;

            expect(eventId).to.be.a.uuid('v4');
        });
    });

    describe('Event data', function() {
        it('should contain a name property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('name');
        });

        describe('Name', function() {
            it('should be a string', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.name).to.be.a('string');
            });
        });

        it('should contain a summary property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('summary');
        });

        describe('Summary', function() {
            it('should be a string', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.summary).to.be.a('string');
            });
        });

        it('should contain a locationId property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('locationId');
        });

        describe('Location', function() {
            it('should be a valid UUID if provided', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                    locationId: uuid(),
                });

                expect(event.data.locationId).to.be.a.uuid('v4');
            });

            it('should be null if not provided', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.locationId).to.be.null;
            });
        });

        it('should contain a time property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('time');
        });

        describe('Time', function() {
            it('should be a Date if provided', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                    time: '2018-05-20T14:15:07.998Z',
                });

                expect(event.data.time).to.be.a('date');
            });

            it('should be null if not provided', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.time).to.be.null;
            });
        });

        it('should contain a participants property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('participants');
        });

        describe('Participants', function() {
            it('should be an array of objects', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.participants).to.be.an('array');
                event.data.participants.forEach(participant => {
                    expect(participant).to.be.an('object');
                });
            });

            it('should contain only the organizer', async function() {
                const mockParticipant = {
                    personId: uuid(),
                    role: participantRole.organizer,
                };

                const event = await createEvent(mockParticipant.personId, 'Test event', {
                });

                expect(event.data.participants).to.have.lengthOf(1);
                expect(event.data.participants[0]).to.deep.equal(mockParticipant);
            });
        });

        it('should contain a createdAt property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('createdAt');
        });

        describe('Creation time', function() {
            it('should be a Date', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.createdAt).to.be.a('date');
            });
        });

        it('should contain a updatedAt property', async function() {
            const event = await createEvent(uuid(), 'Test event', {
            });

            expect(event.data).to.have.property('updatedAt');
        });

        describe('Update time', function() {
            it('should be a Date', async function() {
                const event = await createEvent(uuid(), 'Test event', {
                });

                expect(event.data.updatedAt).to.be.a('date');
            });
        });
    });
});
