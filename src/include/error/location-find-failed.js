'use strict';

class LocationFindFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, LocationFindFailedError);
    }
}

module.exports = LocationFindFailedError;
