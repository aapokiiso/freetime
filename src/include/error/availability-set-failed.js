'use strict';

class AvailabilitySetFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, AvailabilitySetFailedError);
    }
}

module.exports = AvailabilitySetFailedError;
