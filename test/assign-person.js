'use strict';

/* eslint-disable no-magic-numbers */

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
chai.use(require('chai-as-promised'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const uuid = require('uuid/v4');
const entityKind = require('../src/include/entity-kind');
const assignPersonFactory = require('../src/assign-person');
const createEventFactory = require('../src/create-event');
const createPersonFactory = require('../src/create-person');
const setAvailabilityFactory = require('../src/set-availability');

const EventNotFoundError = require('../src/include/error/event-not-found');
const PersonNotFoundError = require('../src/include/error/person-not-found');
const PersonNotAvailableError = require('../src/include/error/person-not-available');
const PersonAlreadyParticipatingError = require('../src/include/error/person-already-participating');

describe('Assigning a Person to an Event', function() {
    let dsEmulator,
        dsInstance,
        assignPerson,
        createEvent,
        testAvailablePersonId,
        testUnavailablePersonId,
        testEventId;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        dsInstance = new Datastore();
        assignPerson = assignPersonFactory(dsInstance);
        createEvent = createEventFactory(dsInstance);

        const createPerson = createPersonFactory(dsInstance);
        const setAvailability = setAvailabilityFactory(dsInstance);

        do {
            try {
                const [ testAvailablePerson, testUnavailablePerson ] = await Promise.all([
                    createPerson(),
                    createPerson(),
                ]);

                const [ , availablePersonId ] = testAvailablePerson.key.path;
                const [ , unavailablePersonId ] = testUnavailablePerson.key.path;

                await Promise.all([
                    setAvailability(availablePersonId, true),
                    setAvailability(unavailablePersonId, false),
                ]);

                testAvailablePersonId = availablePersonId;
                testUnavailablePersonId = unavailablePersonId;
            } catch (e) {
                console.error(e);
            }
        } while (!testAvailablePersonId || !testUnavailablePersonId);
    });

    beforeEach(async function() {
        testEventId = null;

        do {
            const testEvent = await createEvent(uuid(), 'Test event');
            const [ , eventId ] = testEvent.key.path;

            testEventId = eventId;
        } while (!testEventId);
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should fail if Event ID is not provided', function() {
        return expect(
            assignPerson(undefined, testAvailablePersonId)
        ).to.be.rejectedWith(TypeError, 'Event ID must be provided.');
    });

    it('should fail if Person ID is not provided', function() {
        return expect(
            assignPerson(testEventId)
        ).to.be.rejectedWith(TypeError, 'Person ID must be provided.');
    });

    it('should fail if Event ID is not provided as UUID', function() {
        return expect(
            assignPerson(123, testAvailablePersonId)
        ).to.be.rejectedWith(TypeError, 'Event ID must be provided as an UUID.');
    });

    it('should fail if Person ID is not provided as UUID', function() {
        return expect(
            assignPerson(testEventId, 123)
        ).to.be.rejectedWith(TypeError, 'Person ID must be provided as an UUID.');
    });

    it('should fail if Event to assign Person to does not exist', function() {
        return expect(
            assignPerson(uuid(), testAvailablePersonId)
        ).to.be.rejectedWith(EventNotFoundError);
    });

    it('should fail if Person to assign does not exist', function() {
        return expect(
            assignPerson(testEventId, uuid())
        ).to.be.rejectedWith(PersonNotFoundError);
    });

    it('should fail if Person is not available for events', function() {
        return expect(
            assignPerson(testEventId, testUnavailablePersonId)
        ).to.be.rejectedWith(PersonNotAvailableError);
    });

    it('should add Person as an EventParticipant to the Event', async function() {
        await assignPerson(testEventId, testAvailablePersonId);

        const eventKey = dsInstance.key([ entityKind.event, testEventId ]);
        const [event] = await dsInstance.get(eventKey);
        const personIds = event.participants.map(participant => participant.personId);

        expect(personIds).to.include(testAvailablePersonId);
    });

    it('should fail if Person is already an EventParticipant in the Event', async function() {
        await assignPerson(testEventId, testAvailablePersonId);

        return expect(
            assignPerson(testEventId, testAvailablePersonId)
        ).to.be.rejectedWith(PersonAlreadyParticipatingError);
    });

    it('should return nothing', async function() {
        const result = await assignPerson(testEventId, testAvailablePersonId);

        return expect(result).to.be.undefined;
    });
});
