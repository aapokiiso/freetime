'use strict';

class MissingAvailabilityTypeError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, MissingAvailabilityTypeError);
    }
}

module.exports = MissingAvailabilityTypeError;
