'use strict';

class MissingLocationIdError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, MissingLocationIdError);
    }
}

module.exports = MissingLocationIdError;
