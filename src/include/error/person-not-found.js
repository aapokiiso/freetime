'use strict';

class PersonNotFoundError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, PersonNotFoundError);
    }
}

module.exports = PersonNotFoundError;
