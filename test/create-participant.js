'use strict';

const { describe, it } = require('mocha');

const chai = require('chai');
const { expect } = chai;

const uuid = require('uuid/v4');
const createParticipant = require('../src/create-participant');
const role = require('../src/include/participant-role');

describe('Creating an EventParticipant', function() {
    it('should contain a personId property', function() {
        const participant = createParticipant(uuid());

        expect(participant).to.have.property('personId');
    });

    it('should contain a role property', function() {
        const participant = createParticipant(uuid());

        expect(participant).to.have.property('role');
    });

    describe('Role', function() {
        it('should be organizer if isOrganizer flag is set', function() {
            const participant = createParticipant(uuid(), true);

            expect(participant.role).to.equal(role.organizer);
        });

        it('should be participant if isOrganizer flag is not set', function() {
            const participant = createParticipant(uuid());

            expect(participant.role).to.equal(role.participant);
        });
    });
});
