'use strict';

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const createPersonFactory = require('../src/create-person');

describe('Creating a Person', function() {
    let dsEmulator,
        createPerson;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        const dsInstance = new Datastore();
        createPerson = createPersonFactory(dsInstance);
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should return a Person with a key property', async function() {
        const person = await createPerson();

        expect(person).to.have.property('key');
    });

    it('should return a Person with a data property', async function() {
        const person = await createPerson();

        expect(person).to.have.property('data');
    });

    describe('Person key (GCP Datastore)', function() {
        it('should be of the Person kind', async function() {
            const person = await createPerson();
            const { kind } = person.key;

            expect(kind).to.equal('Person');
        });

        it('should only contain kind and a UUID', async function() {
            const person = await createPerson();

            // eslint-disable-next-line no-magic-numbers
            expect(person.key.path.length).to.equal(2);
        });

        it('should contain a valid random v4 UUID', async function() {
            const person = await createPerson();
            const [ kind, personId ] = person.key.path;

            expect(personId).to.be.a.uuid('v4');
        });
    });

    describe('Person data', function() {
        it('should contain an isAvailableForEvents property', async function() {
            const person = await createPerson();

            expect(person.data).to.have.property('isAvailableForEvents');
        });

        describe('Event availability', function() {
            it('should be false by default', async function() {
                const person = await createPerson();

                expect(person.data.isAvailableForEvents).to.be.a('boolean');
                expect(person.data.isAvailableForEvents).to.equal(false);
            });
        });
    });
});
