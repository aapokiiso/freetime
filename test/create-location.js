'use strict';

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
chai.use(require('chai-as-promised'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const uuid = require('uuid/v4');
const createLocationFactory = require('../src/create-location');

describe('Creating an EventLocation', function() {
    let dsEmulator,
        createLocation;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        const dsInstance = new Datastore();
        createLocation = createLocationFactory(dsInstance);
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should fail if name is not provided', function() {
        return expect(
            createLocation()
        ).to.be.rejectedWith(TypeError, 'Event location name must be provided.');
    });

    it('should fail if location is not provided as coordinates object', function() {
        return expect(
            createLocation('Test location', {
                place: 'Helsinki',
            })
        ).to.be.rejectedWith(TypeError, 'Event location must be provided as coordinates object (latitude & longitude).');
    });

    it('should return a EventLocation with a key property', async function() {
        const location = await createLocation(uuid(), {
            latitude: 0,
            longitude: 0,
        });

        expect(location).to.have.property('key');
    });

    it('should return a EventLocation with a data property', async function() {
        const location = await createLocation(uuid(), {
            latitude: 0,
            longitude: 0,
        });

        expect(location).to.have.property('data');
    });

    describe('EventLocation key (GCP Datastore)', function() {
        it('should be of the EventLocation kind', async function() {
            const location = await createLocation('Test location', {
                latitude: 0,
                longitude: 0,
            });
            const { kind } = location.key;

            expect(kind).to.equal('EventLocation');
        });

        it('should only contain kind and a UUID', async function() {
            const location = await createLocation('Test location', {
                latitude: 0,
                longitude: 0,
            });

            // eslint-disable-next-line no-magic-numbers
            expect(location.key.path.length).to.equal(2);
        });

        it('should contain a valid random v4 UUID', async function() {
            const location = await createLocation('Test location', {
                latitude: 0,
                longitude: 0,
            });
            const [ kind, locationId ] = location.key.path;

            expect(locationId).to.be.a.uuid('v4');
        });
    });

    describe('EventLocation data', function() {
        it('should contain a name property', async function() {
            const location = await createLocation('Test location', {
                latitude: 0,
                longitude: 0,
            });

            expect(location.data).to.have.property('name');
        });

        describe('Name', function() {
            it('should be a string', async function() {
                const location = await createLocation('Test location', {
                    latitude: 0,
                    longitude: 0,
                });

                expect(location.data.name).to.be.a('string');
            });
        });

        it('should contain a coordinates property', async function() {
            const location = await createLocation('Test location', {
                latitude: 0,
                longitude: 0,
            });

            expect(location.data).to.have.property('coordinates');
        });

        describe('Coordinates', function() {
            it('should be a Datastore GeoPoint', async function() {
                const location = await createLocation('Test location', {
                    latitude: 0,
                    longitude: 0,
                });

                expect(Datastore.isGeoPoint(location.data.coordinates)).to.be.true;
            });
        });
    });
});
