'use strict';

class LocationNotFoundError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, LocationNotFoundError);
    }
}

module.exports = LocationNotFoundError;
