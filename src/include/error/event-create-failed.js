'use strict';

class EventCreateFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, EventCreateFailedError);
    }
}

module.exports = EventCreateFailedError;
