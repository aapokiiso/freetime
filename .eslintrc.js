module.exports = {
    extends: '@lamiaoy/eslint-config',
    rules: {
        'array-bracket-spacing': ['error', 'always', {
            singleValue: false
        }],
        'space-before-function-paren': ['error', {
            anonymous: 'never',
            named: 'never',
            asyncArrow: 'always',
        }],
    }
};
