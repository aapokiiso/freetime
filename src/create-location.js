'use strict';

const uuid = require('uuid/v4');

const entityKind = require('./include/entity-kind');
const LocationCreateFailedError = require('./include/error/location-create-failed');

module.exports = function createLocationFactory(datastore) {
    return async function createLocation(name, { latitude, longitude } = {}) {
        if (!name) {
            throw new TypeError('Event location name must be provided.');
        }

        if (!(typeof latitude === 'number' && typeof longitude === 'number')) {
            throw new TypeError('Event location must be provided as coordinates object (latitude & longitude).');
        }

        const coordinates = datastore.geoPoint({ latitude, longitude });

        const key = datastore.key([ entityKind.eventLocation, uuid() ]);
        const location = {
            key,
            data: {
                name,
                coordinates,
            },
        };

        try {
            await datastore.save(location);
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new LocationCreateFailedError(message);
        }

        return location;
    };
};
