'use strict';

class LocationRemoveFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, LocationRemoveFailedError);
    }
}

module.exports = LocationRemoveFailedError;
