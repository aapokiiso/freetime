'use strict';

const uuid = require('uuid/v4');
const entityKind = require('./include/entity-kind');
const PersonCreateFailedError = require('./include/error/person-create-failed');

module.exports = function createPersonFactory(datastore) {
    return async function createPerson() {
        const key = datastore.key([ entityKind.person, uuid() ]);

        const person = {
            key,
            data: {
                isAvailableForEvents: false,
            },
        };

        try {
            await datastore.save(person);
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new PersonCreateFailedError(message);
        }

        return person;
    };
};
