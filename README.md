# Freetime

Matching people to go do stuff together.

## Setting up locally

### Prerequisites

- Node.js 8.11.1 or later
- Google Cloud SDK
    - Commands for `gcloud beta emulators`
- Serverless framework's CLI tool

### Testing

Run the tests with
```bash
$ npm run test
```
