'use strict';

const entityKind = require('./include/entity-kind');
const PersonNotFoundError = require('./include/error/person-not-found');
const MissingAvailabilityTypeError = require('./include/error/missing-availability-type');
const AvailabilitySetFailedError = require('./include/error/availability-set-failed');

module.exports = function setAvailabilityFactory(datastore) {
    return async function setAvailability(personId, availability) {
        if (typeof availability === 'undefined') {
            throw new MissingAvailabilityTypeError('Availability to set must be provided.');
        }

        const key = datastore.key([ entityKind.person, personId ]);
        const [person] = await datastore.get(key);

        if (!person) {
            throw new PersonNotFoundError('Could not find person to set availability to.');
        }

        person.isAvailableForEvents = availability;

        try {
            await datastore.update({
                key,
                data: person,
            });
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new AvailabilitySetFailedError(message);
        }
    };
};
