'use strict';

class EventNotFoundError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, EventNotFoundError);
    }
}

module.exports = EventNotFoundError;
