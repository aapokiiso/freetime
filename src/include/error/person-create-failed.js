'use strict';

class PersonCreateFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, PersonCreateFailedError);
    }
}

module.exports = PersonCreateFailedError;
