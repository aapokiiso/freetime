'use strict';

/**
 * NOTICE OF LICENSE
 *
 * This source file is released under commercial license by Lamia Oy.
 *
 * @copyright Copyright (c) 2018 Lamia Oy (https://lamia.fi)
 */

module.exports = {
    person: 'Person',
    event: 'Event',
    eventLocation: 'EventLocation',
    eventParticipant: 'EventParticipant',
};
