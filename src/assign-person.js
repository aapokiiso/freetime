'use strict';

const isUUID = require('./include/is-uuid');
const entityKind = require('./include/entity-kind');
const EventNotFoundError = require('./include/error/event-not-found');
const PersonNotFoundError = require('./include/error/person-not-found');
const PersonNotAvailableError = require('./include/error/person-not-available');
const PersonAssignFailedError = require('./include/error/person-assign-failed');
const PersonAlreadyParticipatingError = require('./include/error/person-already-participating');
const createParticipant = require('./create-participant');

function assignPersonFactory(datastore) {
    return async function assignPerson(eventId, personId) {
        if (!eventId) {
            throw new TypeError('Event ID must be provided.');
        }

        if (!personId) {
            throw new TypeError('Person ID must be provided.');
        }

        if (!isUUID(eventId)) {
            throw new TypeError('Event ID must be provided as an UUID.');
        }

        if (!isUUID(personId)) {
            throw new TypeError('Person ID must be provided as an UUID.');
        }

        const eventKey = datastore.key([ entityKind.event, eventId ]);
        const [event] = await datastore.get(eventKey);
        if (!event) {
            throw new EventNotFoundError('Could not find event to assign person to.');
        }

        const personKey = datastore.key([ entityKind.person, personId ]);
        const [person] = await datastore.get(personKey);
        if (!person) {
            throw new PersonNotFoundError('Could not find person to assign to event.');
        }

        if (!person.isAvailableForEvents) {
            throw new PersonNotAvailableError('Given person is not available for events.');
        }

        if (isAlreadyParticipating(event.participants, personId)) {
            throw new PersonAlreadyParticipatingError('Given person is already participating to the event.');
        }

        const eventParticipant = createParticipant(personId);
        event.participants.push(eventParticipant);

        try {
            await datastore.save({
                key: eventKey,
                data: event,
            });
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new PersonAssignFailedError(message);
        }
    };

    function isAlreadyParticipating(participants, personId) {
        return participants
            .map(participant => participant.personId)
            .includes(personId);
    }
}
module.exports = assignPersonFactory;
