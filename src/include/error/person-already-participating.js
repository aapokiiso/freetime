'use strict';

class PersonAlreadyParticipatingError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, PersonAlreadyParticipatingError);
    }
}

module.exports = PersonAlreadyParticipatingError;
