'use strict';

class MissingLocationCoordinatesError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, MissingLocationCoordinatesError);
    }
}

module.exports = MissingLocationCoordinatesError;
