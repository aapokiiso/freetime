'use strict';

class PersonAssignFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, PersonAssignFailedError);
    }
}

module.exports = PersonAssignFailedError;
