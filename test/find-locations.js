'use strict';

/* eslint-disable no-magic-numbers */

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
chai.use(require('chai-as-promised'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const findLocationsFactory = require('../src/find-locations');
const createLocationFactory = require('../src/create-location');
const MissingLocationCoordinatesError = require('../src/include/error/missing-location-coordinates');

describe('Finding EventLocations', function() {
    let dsEmulator,
        findLocations,
        createLocation;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        const dsInstance = new Datastore();
        findLocations = findLocationsFactory(dsInstance);
        createLocation = createLocationFactory(dsInstance);

        const testLocations = [
            { name: 'Test location 1', coordinates: { latitude: 0, longitude: 90 } },
            { name: 'Test location 2', coordinates: { latitude: 0, longitude: 90 } },
            { name: 'Test location 3', coordinates: { latitude: 0, longitude: 90 } },
            { name: 'Example location 3', coordinates: { latitude: -90, longitude: 0 } },
        ];
        let testLocationsCreated = false;
        do {
            try {
                await Promise.all(
                    testLocations
                        .map(({ name, coordinates }) => createLocation(name, coordinates))
                );
                testLocationsCreated = true;
            } catch (e) {
                console.error(e);
            }
        } while (!testLocationsCreated);
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should fail if search coordinates are not provided', function() {
        return expect(
            findLocations()
        ).to.be.rejectedWith(MissingLocationCoordinatesError);
    });

    it('should return all matching search results, not just the first', async function() {
        const locations = await findLocations(0, 90);

        expect(locations).to.have.lengthOf.above(1);
    });

    it('should return only one result when no others match', async function() {
        const locations = await findLocations(-90, 0);

        expect(locations).to.have.lengthOf(1);
    });

    it('should return no results when none match the query', async function() {
        const locations = await findLocations(90, 90);

        expect(locations).to.be.empty;
    });

    it('should return results only up to the specified limit', async function() {
        const locations = await findLocations(0, 90, { limit: 2 });

        expect(locations).to.have.lengthOf(2);
    });

    it('should return results starting from a specified offset', async function() {
        const firstTwoLocations = await findLocations(0, 90, { limit: 2 });
        const [secondLocation] = await findLocations(0, 90, { limit: 1, offset: 1 });

        expect(firstTwoLocations[1]).to.deep.equal(secondLocation);
    });
});
