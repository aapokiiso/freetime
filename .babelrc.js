module.exports = {
    presets: [
        [
            '@babel/env',
            {
                targets: {
                    // Google Cloud Function is using Node LTS
                    node: '8.11.1',
                }
            }
        ]
    ],
};
