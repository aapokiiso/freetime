'use strict';

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
chai.use(require('chai-as-promised'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const removeLocationFactory = require('../src/remove-location');
const createLocationFactory = require('../src/create-location');
const LocationNotFoundError = require('../src/include/error/location-not-found');
const MissingLocationIdError = require('../src/include/error/missing-location-id');

describe('Removing an EventLocation', function() {
    let dsEmulator,
        removeLocation,
        createLocation,
        testLocationId;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        const dsInstance = new Datastore();
        removeLocation = removeLocationFactory(dsInstance);
        createLocation = createLocationFactory(dsInstance);
    });

    beforeEach(async function() {
        do {
            try {
                const testLocation = await createLocation('Test location 1', {
                    latitude: 0,
                    longitude: 0,
                });

                const [ kind, locationId ] = testLocation.key.path;
                testLocationId = locationId;
            } catch (e) {
                console.error(e);
            }
        } while (!testLocationId);
    });

    afterEach(async function() {
        try {
            await removeLocation(testLocationId);
        } catch (e) {
            // Might've been removed by the test already
        }

        testLocationId = undefined;
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should fail if ID is not provided', function() {
        return expect(
            removeLocation()
        ).to.be.rejectedWith(MissingLocationIdError);
    });

    it('should fail if EventLocation to remove does not exist', async function() {
        await removeLocation(testLocationId);

        return expect(
            removeLocation(testLocationId)
        ).to.be.rejectedWith(LocationNotFoundError);
    });

    it('should return nothing', async function() {
        const result = await removeLocation(testLocationId);

        expect(result).to.be.undefined;
    });
});
