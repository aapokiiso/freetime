'use strict';

const role = require('./include/participant-role');

function createParticipant(personId, isOrganizer = false) {
    const participantRole = isOrganizer ? role.organizer : role.participant;

    return {
        personId,
        role: participantRole,
    };
}

module.exports = createParticipant;
