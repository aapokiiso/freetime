'use strict';

const geolib = require('geolib');
const Datastore = require('@google-cloud/datastore');

const entityKind = require('./include/entity-kind');
const MissingLocationCoordinatesError = require('./include/error/missing-location-coordinates');
const LocationFindFailedError = require('./include/error/location-find-failed');

const defaultSearchLimit = 10;

module.exports = function findLocationsFactory(datastore) {
    return async function findLocations(latitude, longitude, { limit = defaultSearchLimit, offset = 0 } = {}) {
        if (typeof latitude === 'undefined' || typeof longitude === 'undefined') {
            throw new MissingLocationCoordinatesError('Location search coordinates must be provided.');
        }

        try {
            const locations = await findCloseByLocations(latitude, longitude, limit, offset);

            return locations;
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new LocationFindFailedError(message);
        }
    };

    async function findCloseByLocations(latitude, longitude, limit, offset) {
        // GeoPoint filtering is yet not possible, have to filter in application
        // See: https://stackoverflow.com/q/50544306/1495971

        const closeByLocations = [];
        const offsetCloseByLocations = [];
        let shouldContinueQuery;
        let pageCursor = '';

        do {
            const [ locations, info ] = await queryLocations(pageCursor);

            locations
                .filter(location => {
                    const { latitude: locationLatitude, longitude: locationLongitude } = location.coordinates;

                    return areCloseLocations(latitude, longitude, locationLatitude, locationLongitude);
                })
                .forEach(location => {
                    if (offsetCloseByLocations.length < offset) {
                        offsetCloseByLocations.push(location);
                    } else if (closeByLocations.length < limit) {
                        closeByLocations.push(location);
                    }
                });

            const foundLocations = locations.length > 0;
            const moreLocationsMightExist = info.moreResults !== Datastore.NO_MORE_RESULTS;
            const closeByLocationsLimitReached = closeByLocations.length >= limit;

            shouldContinueQuery = foundLocations && moreLocationsMightExist && !closeByLocationsLimitReached;
            pageCursor = info.endCursor;
        } while (shouldContinueQuery);

        return closeByLocations;
    }

    function queryLocations(pageCursor) {
        const pageSize = 100;

        let query = datastore.createQuery(entityKind.eventLocation)
            .limit(pageSize);

        if (pageCursor) {
            query = query.start(pageCursor);
        }

        return datastore.runQuery(query);
    }

    function areCloseLocations(latitudeA, longitudeA, latitudeB, longitudeB) {
        const closenessThresholdInMeters = 100;

        return geolib.getDistance(
            { latitude: latitudeA, longitude: longitudeA },
            { latitude: latitudeB, longitude: longitudeB }
        ) <= closenessThresholdInMeters;
    }

};
