'use strict';

class PersonNotAvailableError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, PersonNotAvailableError);
    }
}

module.exports = PersonNotAvailableError;
