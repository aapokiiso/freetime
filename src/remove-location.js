'use strict';

const entityKind = require('./include/entity-kind');
const LocationNotFoundError = require('./include/error/location-not-found');
const MissingLocationIdError = require('./include/error/missing-location-id');
const LocationRemoveFailedError = require('./include/error/location-remove-failed');

module.exports = function removeLocationFactory(datastore) {
    return async function removeLocation(locationId) {
        if (!locationId) {
            throw new MissingLocationIdError('ID of EventLocation to remove must be provided.');
        }

        const key = datastore.key([ entityKind.eventLocation, locationId ]);

        const [location] = await datastore.get(key);
        if (!location) {
            throw new LocationNotFoundError('EventLocation to remove was not found.');
        }

        try {
            await datastore.delete(key);
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new LocationRemoveFailedError(message);
        }
    };
};
