'use strict';

const uuid = require('uuid/v4');

const entityKind = require('./include/entity-kind');
const createParticipant = require('./create-participant');
const isISOString = require('./include/is-iso-string');
const isUUID = require('./include/is-uuid');

const EventCreateFailedError = require('./include/error/event-create-failed');

module.exports = function createEventFactory(datastore) {
    return async function createEvent(personId, name, {
        summary = '',
        time: dateStr,
        locationId = null,
    } = {}) {
        if (!name) {
            throw new TypeError('Event name must be provided.');
        }

        if (dateStr && !isISOString(dateStr)) {
            throw new TypeError('Event time must be provided as a ISO-8601 string.');
        }

        if (locationId && !isUUID(locationId)) {
            throw new TypeError('EventLocation ID must be provided as an UUID.');
        }

        const key = datastore.key([ entityKind.event, uuid() ]);
        const organizer = createParticipant(personId, true);

        const event = {
            key,
            data: {
                name,
                summary,
                locationId,
                time: dateStr ? new Date(dateStr) : null,
                participants: [organizer],
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        };

        try {
            await datastore.save(event);
        } catch (e) {
            const message = (e instanceof Error) ? e.message : null;
            throw new EventCreateFailedError(message);
        }

        return event;
    };
};
