'use strict';

const Datastore = require('@google-cloud/datastore');
const status = require('http-status');
const createPersonFactory = require('./src/create-person');
const setAvailabilityFactory = require('./src/set-availability');
const createEventFactory = require('./src/create-event');
const findLocationsFactory = require('./src/find-locations');
const createLocationFactory = require('./src/create-location');
const assignPersonFactory = require('./src/assign-person');

module.exports.createPerson = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    try {
        const dsInstance = new Datastore();
        const createPerson = createPersonFactory(dsInstance);

        const person = await createPerson();
        const [ kind, uuid ] = person.key.path;
        response.status(status.OK).send(uuid);
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.setPersonAvailable = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    if (request.get('content-type') !== 'application/json') {
        return response.status(status.UNSUPPORTED_MEDIA_TYPE).send();
    }

    try {
        const dsInstance = new Datastore();
        const setAvailability = setAvailabilityFactory(dsInstance);

        const {
            personId,
        } = request.body;

        await setAvailability(personId, true);
        response.status(status.OK).send();
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.setPersonUnavailable = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    if (request.get('content-type') !== 'application/json') {
        return response.status(status.UNSUPPORTED_MEDIA_TYPE).send();
    }

    try {
        const dsInstance = new Datastore();
        const setAvailability = setAvailabilityFactory(dsInstance);

        const {
            personId,
        } = request.body;

        await setAvailability(personId, false);
        response.status(status.OK).send();
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.createEvent = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    if (request.get('content-type') !== 'application/json') {
        return response.status(status.UNSUPPORTED_MEDIA_TYPE).send();
    }

    try {
        const dsInstance = new Datastore();
        const createEvent = createEventFactory(dsInstance);

        const {
            personId,
            locationId,
            name,
            summary,
            time,
        } = request.body;

        const event = await createEvent(personId, name, {
            summary,
            time,
            locationId,
        });
        const [ kind, uuid ] = event.key.path;
        response.status(status.OK).send(uuid);
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.findLocations = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    if (request.get('content-type') !== 'application/json') {
        return response.status(status.UNSUPPORTED_MEDIA_TYPE).send();
    }

    try {
        const dsInstance = new Datastore();
        const findLocations = findLocationsFactory(dsInstance);

        const {
            latitude,
            longitude,
        } = request.body;

        const {
            limit,
            offset,
        } = request.query;

        const locations = await findLocations(latitude, longitude, {
            limit,
            offset,
        });

        response.status(status.OK).json(locations);
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.createLocation = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    if (request.get('content-type') !== 'application/json') {
        return response.status(status.UNSUPPORTED_MEDIA_TYPE).send();
    }

    try {
        const dsInstance = new Datastore();
        const createLocation = createLocationFactory(dsInstance);

        const {
            name,
            latitude,
            longitude,
        } = request.body;

        const location = await createLocation(name, {
            latitude,
            longitude,
        });
        const [ kind, uuid ] = location.key.path;
        response.status(status.OK).send(uuid);
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.assignPersonToEvent = async (request, response) => {
    if (request.method !== 'POST') {
        return response.status(status.METHOD_NOT_ALLOWED).send();
    }

    if (request.get('content-type') !== 'application/json') {
        return response.status(status.UNSUPPORTED_MEDIA_TYPE).send();
    }

    try {
        const dsInstance = new Datastore();
        const assignPerson = assignPersonFactory(dsInstance);

        const {
            eventId,
            personId,
        } = request.body;

        await assignPerson(eventId, personId);
        response.status(status.OK).send();
    } catch (e) {
        response.status(status.INTERNAL_SERVER_ERROR).send(e.message);
    }
};

module.exports.event = (event, callback) => {
    callback();
};
