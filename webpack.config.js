'use strict';

const path = require('path');
const slsw = require('serverless-webpack');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    entry: './index.js',
    target: 'node',
    externals: [nodeExternals()],
    output: {
        libraryTarget: 'this',
        path: path.join(__dirname, '.webpack'),
        filename: 'index.js',
    },
    mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                ],
            },
        ],
    },
};
