'use strict';

const { describe, it } = require('mocha');

const chai = require('chai');
chai.use(require('chai-uuid'));
const { expect } = chai;

const Datastore = require('@google-cloud/datastore');
const DatastoreEmulator = require('google-datastore-emulator');

const uuid = require('uuid/v4');
const createPersonFactory = require('../src/create-person');
const setAvailabilityFactory = require('../src/set-availability');
const PersonNotFoundError = require('../src/include/error/person-not-found');
const MissingAvailabilityTypeError = require('../src/include/error/missing-availability-type');

describe('Setting the EventAvailability of a Person', function() {
    let dsEmulator,
        createPerson,
        setAvailability,
        testPersonId;

    before(async function() {
        dsEmulator = new DatastoreEmulator();
        await dsEmulator.start();

        const dsInstance = new Datastore();
        createPerson = createPersonFactory(dsInstance);
        setAvailability = setAvailabilityFactory(dsInstance);

        do {
            try {
                const testPerson = await createPerson();

                const [ kind, personId ] = testPerson.key.path;
                testPersonId = personId;
            } catch (e) {
                console.error(e);
            }
        } while (!testPersonId);
    });

    after(function() {
        return dsEmulator.stop();
    });

    it('should fail if Person does not exist', function() {
        const fakePersonId = uuid();

        return expect(
            setAvailability(fakePersonId, true)
        ).to.be.rejectedWith(PersonNotFoundError);
    });

    it('should fail if to-be EventAvailability is not explicitly given', function() {
        return expect(
            setAvailability(testPersonId)
        ).to.be.rejectedWith(MissingAvailabilityTypeError);
    });

    it('should return nothing if successful', async function() {
        const result = await setAvailability(testPersonId, true);

        expect(result).to.be.undefined;
    });
});
