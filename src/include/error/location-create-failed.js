'use strict';

class LocationCreateFailedError extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, LocationCreateFailedError);
    }
}

module.exports = LocationCreateFailedError;
